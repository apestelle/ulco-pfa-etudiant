{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Text
import Database.SQLite.Simple
import GHC.Generics

data Artist = Artist {
    artist_id   :: Int,
    artist_name :: Text
} deriving (Generic, Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field

selectAllArtist :: Connection -> IO [Artist]
selectAllArtist conn = query_ conn "SELECT artist_id, artist_name FROM artist"

main :: IO ()
main = withConnection "music.db" selectAllArtist >>= mapM_ print