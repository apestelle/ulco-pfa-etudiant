{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllTitles :: Connection -> IO [(Text,Text)]
selectAllTitles conn = query_ conn "SELECT artist_name, title_name FROM title \
\INNER JOIN artist ON title_artist = artist_id"

main :: IO ()
main = withConnection "music.db" selectAllTitles >>= mapM_ print