{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Control.Monad.IO.Class
import Database.Selda.SQLite 
import Web.Scotty
import Lucid

import Movie

dbFilename :: String
dbFilename = "movie.db"

main :: IO ()
main = scotty 3000 $ do

    get "/" $ do
        movies <- liftIO $ withSQLite dbFilename dbSelectAllMovies
        html $ renderText $ mkPage movies
        
mkPage :: [Movie] -> Html ()
mkPage movies = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "movies"
        body_ $ do
            h1_ "movies"
            ul_ $ forM_ movies $ \m -> li_ $ do
                toHtml $ movie_title m
                " ("
                toHtml $ show $ movie_year m
                ")