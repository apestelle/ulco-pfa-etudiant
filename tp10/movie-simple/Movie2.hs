{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Movie2 where

import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)

data Movie = Movie
    { movie_id    :: Int
    , movie_title :: Text
    , movie_year :: Int
    } deriving (Generic, Show)

data Prod = Prod
    { _movie :: Text
    , _year :: Int
    , _person :: Text
    , _role :: Text
    } deriving (Generic, Show)


instance FromRow Movie where
    fromRow = Movie <$> field <*> field <*> field

instance FromRow Prod where
    fromRow = Prod <$> field <*> field <*> field <*> field

dbSelectAllMovies :: Connection -> IO [Movie]
dbSelectAllMovies conn = query_ conn
    "SELECT * FROM movie"

dbSelectAllProds :: Connection -> IO [Prod]
dbSelectAllProds conn = query_ conn
    "SELECT movie_title, movie_year, person_name, role_name FROM prod \
    \INNER JOIN movie ON prod_movie = movie_id \
    \INNER JOIN person ON prod_person = person_id \
    \INNER JOIN role ON prod_role = role_id"

dbSelectMovieFromPersonId :: Connection -> Int -> IO [Movie]
dbSelectMovieFromPersonId conn id_movie = query conn
    "SELECT movie_id, movie_title, movie_year FROM prod \
    \INNER JOIN movie ON prod_movie = movie_id \
    \WHERE prod_person = (?)" 
    (Only id_movie)
