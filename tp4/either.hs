type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
mySqrt x = if x < 0 then Left "negative sqrt" else Right (sqrt x)

myLog :: Double -> MyNum
myLog x = if x < 0 then Left "negative log" else Right (log x)

myMul2 :: Double  -> MyNum
myMul2 = return . (*2)

myNeg :: Double -> MyNum
myNeg x = Right (-x)

myCompute :: MyNum
myCompute = mySqrt 16 >>= myNeg >>= myMul2

main :: IO ()
main = do
    print $ mySqrt 42.0
    print $ mySqrt (-10.0)

    print $ myLog 42.0
    print $ myLog (-10.0)

    print $ myMul2 21
    print $ myNeg 21
    putStrLn $ showMyNum myCompute
