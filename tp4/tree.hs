
data Tree a = Leaf | Node a (Tree a) (Tree a)

instance Show a => Show (Tree a) where 
    show (Node val left right) = 
        "(" ++ (show val) ++ (show left) ++ (show right) ++ ")"
    show Leaf = "_"

instance Foldable Tree where 
    foldMap _ Leaf = mempty 
    foldMap f (Node x left right) = foldMap f left `mappend` f x `mappend` foldMap f right

{--
    sum est défini dans foldable
    maximum aussi
--}

mytree1 :: Tree Int
mytree1 = Node 7 (Node 2 Leaf Leaf)
                (Node 37 (Node 13 Leaf Leaf)
                         (Node 42 Leaf Leaf))

mytree2 :: Tree Double
mytree2 = Node 7 (Node 2 Leaf Leaf)
            (Node 37 (Node 13 Leaf Leaf)
                        (Node 42 Leaf Leaf))

main :: IO ()
main = do
    print mytree1
    print mytree2

    print $ sum mytree1
    print $ maximum mytree1

