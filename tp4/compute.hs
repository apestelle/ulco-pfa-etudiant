import Data.List

-- kernel :: Int -> Int
kernel :: Num a => a -> a
kernel = (*2) . (+1)

-- compute :: [Int] -> Int
compute :: Num a => [a] -> a
compute = foldl' (\ acc x -> acc + kernel x) 0

main :: IO ()
main = do
    print $ compute [1..3::Int]
    print $ compute [1..3::Double]

