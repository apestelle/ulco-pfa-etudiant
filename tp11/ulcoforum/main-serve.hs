{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Control.Monad.IO.Class
import Database.SQLite.Simple (open, close)
import Web.Scotty
import Lucid
import Data.Text (Text)


import Thread

dbFilename :: String
dbFilename = "forum.db"

main :: IO ()
main = do
    dbConnexion <- open dbFilename
    scotty 3000 $ do
        get "/" $ do
            html $ renderText $ buildHeader
    
        get "/alldata" $ do
            threadsData <- liftIO $ getAllThreads dbConnexion

            html $ renderText $ printAllData threadsData

        get "/allthreads" $ do
            html $ renderText $ buildHeader 
    
    close dbConnexion

buildHeader :: Html ()
buildHeader = do 
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ulcoForum"
        body_ $ do
            h1_ $ a_ [href_ ("/")] "ulcoforum"
            a_ [href_ ("/alldata")] "ulcoforum - "
            a_ [href_ ("/allthreads")] "allthreads (not available yet)"

printAllData :: [(Int, Text, Text, Int)] -> Html ()
printAllData threads = do
    buildHeader
    ul_ $ forM_ threads $ \(threadId, title, content, authorId) -> li_ $ do
        h1_ $ toHtml title
        p_ $ toHtml content
        p_ $ toHtml  ("authorId : " ++ show authorId)