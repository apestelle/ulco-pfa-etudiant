-- author_id  :: Int,
-- author_firstname ::Text,
-- author_lastname ::Text

CREATE TABLE author (
  author_id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_firstname TEXT,
  author_lastname TEXT
);

CREATE TABLE thread (
    thread_id INTEGER PRIMARY KEY AUTOINCREMENT,
    thread_title TEXT,
    thread_content TEXT,
    author_id INTEGER,
    FOREIGN KEY(author_id) REFERENCES author(author_id)
);

INSERT INTO author VALUES(1, 'Joe', "Claudio");
INSERT INTO author VALUES(2, 'Francis', "isseSauc");
INSERT INTO author VALUES(3, 'Gitto', "LeRipo");

INSERT INTO thread VALUES(1, 'annonce importante', 'Pas de vacances', 1);
INSERT INTO thread VALUES(2, 'random', 'Salut a tous', 2);
INSERT INTO thread VALUES(3, 'Philo et debat', 'a partir de combien de grains de sable peut-on parler d un tas de sable?', 3);

