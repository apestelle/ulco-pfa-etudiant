{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Thread where 
    
import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Thread = Thread{
    thread_id :: Int,
    thread_title :: Text,
    thread_content :: Text,
    author_id ::Int
}

getAllThreads :: Connection -> IO [(Int, Text, Text, Int)]
getAllThreads conn = do
    query_ conn "SELECT * FROM thread"