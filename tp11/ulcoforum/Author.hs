{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Author where 
    
import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)

data Author = Author{
    author_id  :: Int,
    author_firstname ::Text,
    author_lastname ::Text
} deriving(Generic, Show)

