import Data.Char

-- isSorted
isSorted :: (Ord a) => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (a:b:t) = a < b && isSorted (b:t)

-- nocaseCmp 
nocaseCmp :: [Char] -> [Char]-> Bool
nocaseCmp [] [] = True
nocaseCmp [] (_:_) = True 
nocaseCmp (_:_) [] = True 
nocaseCmp (h1:t1) (h2:t2)  = (toLower h1) <= (toLower h2) && (nocaseCmp t1 t2)


main :: IO ()
main = 
    do
        print $ isSorted [1..4]
        print $ isSorted [4,3 ..1]
        print $ isSorted "abc"
        print $ isSorted "foo"

        print $ nocaseCmp "tata" "TOTO"

