
-- mymap1 
myMap1 :: (a -> b) -> [a] -> [b]
myMap1 _ [] = []
myMap1 f (x:xs) = f x : myMap1 f xs

-- mymap2
myMap2 :: (a -> b) -> [a] -> [b]
myMap2 f xs = go xs []
    where 
        go [] acc = acc
        go (h:t) acc = go t (acc ++ [f h])

-- myfilter1 
myFilter1 :: (a -> Bool) -> [a] -> [a]
myFilter1 _ [] = []
myFilter1 f (h:t) 
    | f h = (h:myFilter1 f t)
    | otherwise = myFilter1 f t

-- myfilter2 
myFilter2 :: (a -> Bool) -> [a] -> [a]
myFilter2 _ [] = []
myFilter2 f list = go list []
    where
        go [] acc = acc
        go (h:t) acc 
            | f h = go t (acc ++ [h])
            | otherwise = go t acc

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ myMap1 (2*) [1..5]
    print $ myMap2 (2*) [1..5]
    
    print $ myFilter1 (3>) [1..5]
    print $ myFilter2 (3<) [1..5]

