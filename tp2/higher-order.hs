import Data.List

-- threshList
threshList :: Ord a => a -> [a] -> [a]
threshList s = map (min s)

-- selectList
selectList :: Ord a => a -> [a] -> [a]
selectList selector = filter (< selector)

-- maxList
maxList :: Ord a => [a] -> a
maxList = foldl1 max

main :: IO ()
main = do
    print $ threshList 3 [1..5]
    print $ selectList 3 [1..5]
    print $ maxList [13,42,37]
    print $ maxList ["d","e","z"]

