
-- safeTailString 
safeTailString :: [Char] -> [Char]
safeTailString [] = []
safeTailString (_:t) = t

-- safeHeadString 
safeHeadString :: [Char] -> Maybe Char
safeHeadString [] = Nothing
safeHeadString (h:_) = Just h

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:t) = t

-- safeHead 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (h:_) = Just h

main :: IO ()
main = do
    print $ safeTailString "toto"
    print $ safeHeadString "toto"

    print $ safeTailString ""
    print $ safeHeadString ""

    print $ safeTail "toto"
    print $ safeHead "toto"

    print $ safeTail ""
    print $ safeHead ""


