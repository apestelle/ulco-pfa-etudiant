import Site
import ViewAlbum

main :: IO ()
main = do
    mSites <- loadSites "../data/genalbum.json"
    case mSites of
        Nothing -> print "bad file"
        Just sites -> renderToFile "index.html" $ ViewAlbum sites
