{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Site where

import Data.Aeson
import GHC.Generics

data Site = Site {
    imgs :: [String],
    url :: String
} deriving(Generic, Show)

instance FromJSON Site

loadSites :: FilePath -> IO (Maybe [Site])
loadSites = decodeFileStrict'