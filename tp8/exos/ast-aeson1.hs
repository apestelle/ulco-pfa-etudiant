{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson as A
import Data.ByteString.Lazy.Char8 as BS
import Data.HashMap.Strict

main :: IO ()
main = do
     BS.putStrLn $ A.encode $ A.String "john"
     BS.putStrLn $ A.encode $ A.Number 42
     BS.putStrLn $ A.encode $ A.Object $ fromList [("birthyear", A.Number 42)]
     BS.putStrLn $ A.encode $ A.Object $ fromList 
        [ 
            ("birthyear", A.Number 42),
            ("first", A.String "john"),
            ("last", A.String "doe")
        ]