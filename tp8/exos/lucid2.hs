{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules #-}

import Lucid
import Data.Text.Lazy.IO as T

main :: IO ()
main = do
    T.putStrLn $ renderText monHtml

monHtml :: Html ()
monHtml = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "UTF-8"]
        body_ $ do
            h1_ "Hello!"
            img_ [src_ "toto.png"]
            p_ $ do
                "This is "
                a_ [href_ "toto.png"] ("a link")