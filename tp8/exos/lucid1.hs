{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules #-}

import Lucid
import Data.Text.Lazy.IO as T

main :: IO ()
main = do
    T.putStrLn $ renderText monHtml

monHtml :: Html ()
monHtml = do
    h1_ "Hello"
    p_ "World!"