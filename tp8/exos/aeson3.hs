{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import Data.Aeson
import GHC.Generics

data Address = Address
    {
        road    :: T.Text,
        zipcode :: Int,
        city    :: T.Text,
        number  :: Int
    } deriving (Generic, Show)

data Person = Person
    { 
        firstname    :: T.Text,
        address      :: Address,
        lastname     :: T.Text,
        birthyear    :: Int
    } deriving (Generic, Show)


instance ToJSON Person
instance ToJSON Address

persons :: [Person]
persons =
    [ 
        Person "John" (Address "Pont Vieux" 43000 "Espaly" 42) "Doe" 1970,
        Person "Haskell" (Address "Pere Lachaise" 75000 "Paris" 1337) "Curry" 1900
    ]

main :: IO ()
main = do 
    encodeFile "aeson3.json" persons