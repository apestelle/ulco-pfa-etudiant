-- seuilInt
seuilInt :: Int -> Int -> Int -> Int
seuilInt a b c 
    | c < a = a
    | c > b = b
    | otherwise = c

-- seuilTuple
seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (a,b) c
    | c < a = a
    | c > b = b
    | otherwise = c

main :: IO ()
main = do
    print $ seuilInt 1 10 0
    print $ seuilInt 1 10 2
    print $ seuilInt 1 10 42

    print $ seuilTuple (1, 10) 0
    print $ seuilTuple (1, 10) 2
    print $ seuilTuple (1, 10) 42
