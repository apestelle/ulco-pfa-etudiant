fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (h:t)
    | mod h 15 == 0 = "fizzbuzz" :fizzbuzz1 t
    | mod h 3 == 0  = "fizz" :fizzbuzz1 t
    | mod h 5 == 0  = "buzz" :fizzbuzz1 t
    | otherwise     = show h :fizzbuzz1 t

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 arr =
    fizzbuzz2_Internal arr []
    where
        fizzbuzz2_Internal :: [Int] -> [String] -> [String]
        fizzbuzz2_Internal [] acc = acc
        fizzbuzz2_Internal (head:tail) acc
            | ((mod head 3) == 0) && (mod head 5 == 0) = fizzbuzz2_Internal tail (acc ++ ["fizzbuzz"])
            | ((mod head 3) == 0)                      = fizzbuzz2_Internal tail (acc ++ ["fizz"])
            | ((mod head 5) == 0)                      = fizzbuzz2_Internal tail (acc ++ ["buzz"])
            | otherwise                                = fizzbuzz2_Internal tail (acc ++ [(show head)])

main :: IO ()
main = print $ fizzbuzz1 [1..15]

