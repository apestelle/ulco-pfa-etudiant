import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

saisie :: IO ()
saisie = do
    putStrLn "saisie : "
    n <- getLine
    case readMaybe n of
        Just x -> putStrLn ("Vous avez saisi l'entier " ++ show (x::Int))
        Nothing -> putStrLn "saisie invalide"

main :: IO ()
main = saisie
