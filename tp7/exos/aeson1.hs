{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Aeson as A
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance A.FromJSON Person
instance A.ToJSON Person

main :: IO ()
main = do
    let res0 = Person "John" "Doe" "1970" False
    print res0
    print $ A.encode res0

    parsedValue <- (A.eitherDecodeFileStrict' "aeson-test1.json") 
    print $ (parsedValue :: Either String Person)

    parsedValue2 <- (A.eitherDecodeFileStrict' "aeson-test2.json") 
    print $ (parsedValue2 :: Either String [Person])
    
    parsedValue3 <- (A.eitherDecodeFileStrict' "aeson-test3.json") 
    print $ (parsedValue3 :: Either String [Person])