-- {-# LANGUAGE OverloadedStrings #-}

import Data.Text

newtype Person = Person Text deriving Show

persons :: [Person]
persons = [Person (pack "John"), Person (pack "Haskell")]

main :: IO ()
main = print persons