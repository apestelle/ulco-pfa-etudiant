{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson as A

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
    } deriving (Show)

-- instance FromJSON Person where 
--     parseJSON = withObject "Person" $ \v -> do
--         f <- v .: "firstname"
--         l <- v .: "lastname"
--         b <- read <$> v .: "birthyear"
--         return $ Person f l b

instance A.FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> Person 
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    let res0 = Person "John" "Doe" 1970
    print res0

    parsedValue <- (A.eitherDecodeFileStrict' "aeson-test1.json") 
    print $ (parsedValue :: Either String Person)

    parsedValue2 <- (A.eitherDecodeFileStrict' "aeson-test2.json") 
    print $ (parsedValue2 :: Either String [Person])

    parsedValue3 <- (A.eitherDecodeFileStrict' "aeson-test3.json") 
    print $ (parsedValue3 :: Either String [Person])
