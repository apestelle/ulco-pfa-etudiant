import qualified Data.Text.IO as T
import qualified Data.Text as T

main :: IO ()
main = do
    file <- T.readFile "text2.hs"
    let text = T.unpack file
    putStrLn $ text