{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Yaml as A

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
    , sites    :: [String]
    } deriving (Show)

instance A.FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> Person 
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "sites"

main :: IO ()
main = do
    parsedValue <- (A.decodeFileEither "yaml-test1.yaml") 
    print $ (parsedValue :: Either ParseException Person)