
-- TODO Jour
data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche 

estWeekend :: Jour -> Bool
estWeekend Dimanche = True
estWeekend Samedi = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables = length . filter (not . estWeekend)

main :: IO ()
main = do
    print $ estWeekend Lundi
    print $ estWeekend Samedi
    print $ estWeekend Jeudi
    
    print $ compterOuvrables [ Lundi, Jeudi, Samedi, Dimanche ]
    print $ compterOuvrables [ Samedi, Dimanche ]


