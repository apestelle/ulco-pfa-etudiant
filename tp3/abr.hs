import Data.List (foldl')

data Abr a = Empty | Node a (Abr a) (Abr a) deriving (Show)

-- insererAbr 
insererAbr :: (Show a, Ord a) => Abr a -> a -> Abr a
insererAbr Empty val = (Node val Empty Empty);
insererAbr (Node currVal leftTree rightTree) val
    | val < currVal  = (Node currVal (insererAbr leftTree val) rightTree)
    | otherwise      = (Node currVal leftTree (insererAbr rightTree val))

-- listToAbr 
listToAbr :: (Show a, Ord a) => [a] -> Abr a
listToAbr [] = Empty
listToAbr (h:t) = insererAbr (listToAbr t) h

-- abrToList 
abrToList :: (Show a) => Abr a -> [a]
abrToList Empty = []
abrToList (Node val leftTree rightTree) = (abrToList leftTree) ++ [val] ++ (abrToList rightTree)

main :: IO ()
main = let
    tree = (Node 10 Empty Empty)
    in do
    print tree
    print $ insererAbr tree 11
    print $ insererAbr tree 19 
    print $ insererAbr tree 9

