
-- TODO User
data User = User {
    _firstname :: String,
    _lastname :: String,
    _age :: Int
} 

showUser :: User -> String
showUser user =
    "firstname : " ++ _firstname user ++ " - lastname : " ++ _lastname user ++ " - age : " ++ show (_age user)

incAge :: User -> User
incAge user = user {_age = _age user + 1}

main :: IO ()
main = let
    alexis = User {
        _firstname = "Alexis",
        _lastname = "Pestelle",
        _age = 26
    }
    in do
    print $ showUser alexis
    print $ showUser (incAge alexis)

